# Back to Basics Auxiliary

Auxiliary file project for **Company of Heroes: Back to Basics** modification

- Main project repo: [https://gitlab.com/AGameAnx/back-to-basics](https://gitlab.com/AGameAnx/back-to-basics)
- Steam page: https://store.steampowered.com/app/1527290/Company_of_Heroes_Back_to_Basics/
